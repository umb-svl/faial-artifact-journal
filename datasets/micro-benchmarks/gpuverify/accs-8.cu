//pass
//--blockDim=[64] --gridDim=[1]

#include <cuda.h>






__global__ void kernel (float* out) {



    out[threadIdx.x + 0 * blockDim.x ] = out[threadIdx.x + 15 * blockDim.x ];
    out[threadIdx.x + 1 * blockDim.x ] = out[threadIdx.x + 24 * blockDim.x ];
    out[threadIdx.x + 2 * blockDim.x ] = out[threadIdx.x + 62 * blockDim.x ];
    out[threadIdx.x + 3 * blockDim.x ] = out[threadIdx.x + 25 * blockDim.x ];
    out[threadIdx.x + 4 * blockDim.x ] = out[threadIdx.x + 9 * blockDim.x ];
    out[threadIdx.x + 5 * blockDim.x ] = out[threadIdx.x + 13 * blockDim.x ];
    out[threadIdx.x + 6 * blockDim.x ] = out[threadIdx.x + 46 * blockDim.x ];
    out[threadIdx.x + 7 * blockDim.x ] = out[threadIdx.x + 3 * blockDim.x ];

}
