//pass
//--blockDim=[32,1,1] --gridDim=[4,1,1]

#include <cuda.h>






__global__ void kernel (int* d_ptr, int length) {



    int elemID = blockIdx.x * blockDim.x + threadIdx.x;

    if (elemID < length)
    {
        d_ptr[elemID] = elemID;
    }

}
