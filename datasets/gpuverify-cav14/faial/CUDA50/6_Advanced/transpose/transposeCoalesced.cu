//pass
//--blockDim=[16,16] --gridDim=[64,64]

#include <cuda.h>
#include "common.h"






__global__ void kernel (float* odata, float* idata, int width,int height,int nreps) {

/* kernel pre-conditions */

__requires(height >= blockDim.x);





    __shared__ float tile[TILE_DIM][TILE_DIM];

    int xIndex = blockIdx.x * TILE_DIM + threadIdx.x;
    int yIndex = blockIdx.y * TILE_DIM + threadIdx.y;
    int index_in = xIndex + (yIndex)*width;

    xIndex = blockIdx.y * TILE_DIM + threadIdx.x;
    yIndex = blockIdx.x * TILE_DIM + threadIdx.y;
    int index_out = xIndex + (yIndex)*height;
    __requires (index_in < width);
        for (int r=0; r < nreps; r++)
        {
            for (int i=0; i<TILE_DIM; i+=BLOCK_ROWS)
            {
                tile[threadIdx.y+i][threadIdx.x] = idata[index_in+i*width];
            }

            __syncthreads();

            for (int i=0; i<TILE_DIM; i+=BLOCK_ROWS)
            {
                odata[index_out+i*height] = tile[threadIdx.x][threadIdx.y+i]; /*threadIdx.x*TILE_DIM(16) + threadIdx.y + i*/
            }
        }

}
