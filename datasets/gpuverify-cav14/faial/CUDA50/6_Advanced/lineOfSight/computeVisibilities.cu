//pass
//--blockDim=[256] --gridDim=[40]

#include <cuda.h>

typedef unsigned char Bool;
typedef unsigned int uint;





__global__ void kernel (const float* angles, const float* scannedAngles, Bool* visibilities, int numAngles) {



    uint i = blockDim.x * blockIdx.x + threadIdx.x;

    if (i < numAngles)
    {
        visibilities[i] = scannedAngles[i] <= angles[i];
    }

}
