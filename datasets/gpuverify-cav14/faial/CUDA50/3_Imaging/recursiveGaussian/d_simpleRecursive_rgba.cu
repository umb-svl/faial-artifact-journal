//pass
//--blockDim=[64] --gridDim=[8]

#include <cuda.h>
#include "common.h"






__global__ void kernel (uint* id, uint* od, int w,int h,float a) {

/* kernel pre-conditions */

__requires(w == 512);




    unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;

    __requires(x < w);

    // forward pass
    float4 yp = rgbaIntToFloat(id[x]);  // previous output

    for (int y = 0;
         y < h; y++)
    {
        uint tmp = id[x + y * w];
        float4 xc = rgbaIntToFloat(tmp);
        float4 yc = xc + a*(yp - xc);   // simple lerp between current and previous value
        od[x + y * w] = rgbaFloatToInt(yc);
        yp = yc;
    }

    // reverse pass
    // ensures response is symmetrical
    yp = rgbaIntToFloat(id[x]);

    for (int y = h-1;
         y >= 0; y--)
    {
        uint tmp = id[x + y * w];
        float4 xc = rgbaIntToFloat(tmp);
        float4 yc = xc + a*(yp - xc);
        od[x + y * w] = rgbaFloatToInt((rgbaIntToFloat(od[x + y * w]) + yc)*0.5f);
        yp = yc;
    }

}
