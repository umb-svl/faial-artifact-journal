//pass
//--blockDim=[512,1,1] --gridDim=[32768,1,1]

#include <cuda.h>






__global__ void kernel (float* src, float* dst) {



    // Just a dummy kernel, doing enough for us to verify that everything
    // worked
    const int idx = blockIdx.x * blockDim.x + threadIdx.x;
    dst[idx] = src[idx] * 2.0f;

}
