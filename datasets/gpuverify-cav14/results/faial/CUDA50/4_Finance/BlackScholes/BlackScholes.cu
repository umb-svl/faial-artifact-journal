//pass
//--blockDim=[128] --gridDim=[480]

#include <cuda.h>

__device__ static __attribute__((always_inline)) float cndGPU(float d)
{
    const float       A1 = 0.31938153f;
    const float       A2 = -0.356563782f;
    const float       A3 = 1.781477937f;
    const float       A4 = -1.821255978f;
    const float       A5 = 1.330274429f;
    const float RSQRT2PI = 0.39894228040143267793994605993438f;
    float
    K = 1.0f / (1.0f + 0.2316419f * fabsf(d));
    float
    cnd = RSQRT2PI * __expf(- 0.5f * d * d) *
          (K * (A1 + K * (A2 + K * (A3 + K * (A4 + K * A5)))));
    if (d > 0)
        cnd = 1.0f - cnd;
    return cnd;
}





__global__ void kernel (float* d_CallResult, float* d_PutResult, float* d_StockPrice, float* d_OptionStrike, float* d_OptionYears, float Riskfree,float Volatility,int optN) {

/* kernel pre-conditions */

__requires(optN == 4000000);





    //Thread index
    const int      tid = blockDim.x * blockIdx.x + threadIdx.x;
    //Total number of threads in execution grid
    const int THREAD_N = blockDim.x * gridDim.x;

    //No matter how small is execution grid or how large OptN is,
    //exactly OptN indices will be processed with perfect memory coalescing
    for (int opt = tid;
        opt < optN; opt += THREAD_N) {
        float S = d_StockPrice[opt];
        float X = d_OptionStrike[opt]; //Option strike
        float T = d_OptionYears[opt]; //Option years
        float R = Riskfree; //Riskless rate
        float V = Volatility; //Volatility rate
        float sqrtT, expRT;
        float d1, d2, CNDD1, CNDD2;
        sqrtT = sqrtf(T);
        d1 = (__logf(S / X) + (R + 0.5f * V * V) * T) / (V * sqrtT);
        d2 = d1 - V * sqrtT;
        CNDD1 = cndGPU(d1);
        CNDD2 = cndGPU(d2);
        //Calculate Call and Put simultaneously
        expRT = __expf(- R * T);
        d_CallResult[opt] = S * CNDD1 - X * expRT * CNDD2;
        d_PutResult[opt]  = X * expRT * (1.0f - CNDD2) - S * (1.0f - CNDD1);
    }

}
