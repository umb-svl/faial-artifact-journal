//pass
//--blockDim=[256,1,1] --gridDim=[11377,1,1]

#include <cuda.h>
#include "common.h"






__global__ void kernel (const uint* startpoints, const uint* verticesMapping, const uint* edges, const float* weights, uint* newStartpoints, uint* survivedEdgesIDs, uint edgesCount,uint newVerticesCount) {



    uint tid = blockIdx.x * blockDim.x + threadIdx.x;

    if (tid < edgesCount)
    {
        uint startpoint = startpoints[tid];
        uint endpoint = edges[tid];

        newStartpoints[tid] = endpoint < UINT_MAX ?
                              verticesMapping[startpoint] :
                              newVerticesCount + verticesMapping[startpoint];

        survivedEdgesIDs[tid] = endpoint < UINT_MAX ?
                                tid :
                                UINT_MAX;
    }

}
