#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif
#include "common.h"

#define imageW 320





__global__ void kernel (TColor* dst, int imageH,float Noise,float lerpC) {




__requires(blockDim.x == 8);


__requires(blockDim.y == 8);




__requires(gridDim.x == 40);


__requires(gridDim.y == 51);




const int ix = blockDim.x * blockIdx.x + threadIdx.x;
const int iy = blockDim.y * blockIdx.y + threadIdx.y;
//Add half of a texel to always address exact texel centers
const float x = (float)ix + 0.5f;
const float y = (float)iy + 0.5f;

if (ix < imageW && iy < imageH)
{
    //Normalized counter for the NLM weight threshold
    float fCount = 0;
    //Total sum of pixel weights
    float sumWeights = 0;
    //Result accumulator
    float3 clr = {0, 0, 0};

    //Cycle through NLM window, surrounding (x, y) texel
    for (float i = -NLM_WINDOW_RADIUS; i <= NLM_WINDOW_RADIUS; i++)
        for (float j = -NLM_WINDOW_RADIUS; j <= NLM_WINDOW_RADIUS; j++)
        {
            //Find color distance from (x, y) to (x + j, y + i)
            float weightIJ = 0;

            for (float n = -NLM_BLOCK_RADIUS; n <= NLM_BLOCK_RADIUS; n++)
                for (float m = -NLM_BLOCK_RADIUS; m <= NLM_BLOCK_RADIUS; m++)
                    weightIJ += vecLen(
                                    tex2D(texImage, x + j + m, y + i + n),
                                    tex2D(texImage,     x + m,     y + n)
                                );

            //Derive final weight from color and geometric distance
            weightIJ     = __expf(-(weightIJ * Noise + (i * i + j * j) * INV_NLM_WINDOW_AREA));

            //Accumulate (x + j, y + i) texel color with computed weight
            float4 clrIJ = tex2D(texImage, x + j, y + i);
            clr.x       += clrIJ.x * weightIJ;
            clr.y       += clrIJ.y * weightIJ;
            clr.z       += clrIJ.z * weightIJ;

            //Sum of weights for color normalization to [0..1] range
            sumWeights  += weightIJ;

            //Update weight counter, if NLM weight for current window texel
            //exceeds the weight threshold
            fCount      += (weightIJ > NLM_WEIGHT_THRESHOLD) ? INV_NLM_WINDOW_AREA : 0;
        }

    //Normalize result color by sum of weights
    sumWeights = 1.0f / sumWeights;
    clr.x *= sumWeights;
    clr.y *= sumWeights;
    clr.z *= sumWeights;

    //Choose LERP quotent basing on how many texels
    //within the NLM window exceeded the weight threshold
    float lerpQ = (fCount > NLM_LERP_THRESHOLD) ? lerpC : 1.0f - lerpC;

    //Write final result to global memory
    float4 clr00 = tex2D(texImage, x, y);
    clr.x = lerpf(clr.x, clr00.x, lerpQ);
    clr.y = lerpf(clr.y, clr00.y, lerpQ);
    clr.z = lerpf(clr.z, clr00.z, lerpQ);
    dst[imageW * iy + ix] = make_color(clr.x, clr.y, clr.z, 0);
}
}
