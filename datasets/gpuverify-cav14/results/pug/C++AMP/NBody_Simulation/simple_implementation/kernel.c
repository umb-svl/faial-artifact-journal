#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif

#define TILE_SIZE							256
#define SOFTENING_SQUARED 					0.0000015625f
#define _FG									(6.67300e-11f*10000.0f)
#define F_PARTICLE_MASS						(_FG*10000.0f*10000.0f)
#define DELTA_TIME 							0.1f
#define DAMPENING 							1.0f
#define UINT unsigned int
// GPU based functions
static __attribute__((always_inline))
__device__ void bodybody_interaction(float4 *acc, const float4 my_curr_pos, float4 other_element_old_pos)
{
    float4 r = other_element_old_pos - my_curr_pos;
    float dist_sqr = r.x*r.x + r.y*r.y + r.z*r.z;
    dist_sqr += SOFTENING_SQUARED;
    float inv_dist = rsqrt(dist_sqr);
    float inv_dist_cube =  inv_dist*inv_dist*inv_dist;
    float s = F_PARTICLE_MASS*inv_dist_cube;
    (*acc) += r*s;
}





__global__ void kernel (float4* data_in_pos, float4* data_in_vel, float4* data_out_pos, float4* data_out_vel, unsigned int num_bodies) {




__requires(blockDim.x == 256);





__requires(gridDim.x == 1024);





  int idx = blockIdx.x * blockDim.x + threadIdx.x;
	{
        float4 p_pos;
        float4 p_vel;

        p_pos = data_in_pos[idx];
        p_vel = data_in_vel[idx];
      //float4 acc = (float4)(0, 0, 0, 0);
        float4 acc;
        acc.x = 0; acc.y = 0; acc.z = 0; acc.w = 0;

        // Update current particle using all other particles
        for (UINT j = 0; j < num_bodies; j++) 
        {
	        bodybody_interaction(&acc, p_pos, data_in_pos[j]);
        }

        p_vel += acc*DELTA_TIME;
        p_vel *= DAMPENING;

        p_pos += p_vel*DELTA_TIME;

        data_out_pos[idx] = p_pos;
        data_out_vel[idx] = p_vel;
#ifdef MUTATION
        data_out_vel[idx+1] = data_out_vel[idx+1];
         /* BUGINJECT: ADD_ACCESS, UP */
#endif
	}

}
