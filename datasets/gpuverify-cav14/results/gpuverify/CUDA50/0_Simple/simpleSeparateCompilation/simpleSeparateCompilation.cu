//pass
//--blockDim=[1024,1,1] --gridDim=[1,1,1]

#include <cuda.h>

__device__ float multiplyByTwo(float number)
{
    return number * 2.0f;
}
__device__ float divideByTwo(float number)
{
    return number * 0.5f;
}
typedef unsigned int uint;
typedef float(*deviceFunc)(float);





__global__ void kernel (float* v, deviceFunc f,uint size) {

/* kernel pre-conditions */

__requires(f == multiplyByTwo | f == divideByTwo);




    uint tid = blockIdx.x * blockDim.x + threadIdx.x;

    if (tid < size)
    {
        v[tid] = (*f)(v[tid]);
    }

}
