//pass
//--blockDim=[256,1,1] --gridDim=[1322,1,1]

#include <cuda.h>
#include "common.h"






__global__ void kernel (const uint* successors, uint* representatives, uint verticesCount) {



    uint tid = blockIdx.x * blockDim.x + threadIdx.x;

    if (tid < verticesCount)
    {
        uint successor = successors[tid];
        uint nextSuccessor = successors[successor];

        while (successor != nextSuccessor)
        {
            successor = nextSuccessor;
            nextSuccessor = successors[nextSuccessor];
        }

        representatives[tid] = successor;
    }

}
