//pass
//--blockDim=[256,1,1] --gridDim=[4800,1,1]

#include <cuda.h>
#include "common.h"






__global__ void kernel (const uint* verticesOffsets, uint* flags, uint verticesCount) {



    uint tid = blockIdx.x * blockDim.x + threadIdx.x;

    if (tid < verticesCount)
    {
        int idx = verticesOffsets[tid];
        flags[idx] = 1;
    }

}
