#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif

// N-queen for CUDA
//
// Copyright(c) 2008 Ping-Che Chen

#define THREAD_NUM		96

/* --------------------------------------------------------------------------
* This is a non-recursive version of n-queen backtracking solver for CUDA.
* It receives multiple initial conditions from a CPU iterator, and count
* each conditions.
* --------------------------------------------------------------------------
*/





__global__ void kernel (int* total_masks, int* total_l_masks, int* total_r_masks, int* results, int n,int mark,int total_conditions) {




__requires(blockDim.x == 96);





__requires(gridDim.x == 96);





const int tid = threadIdx.x;
const int bid = blockIdx.x;
const int idx = bid * blockDim.x + tid;

__shared__ unsigned int mask[THREAD_NUM][10];
__shared__ unsigned int l_mask[THREAD_NUM][10];
__shared__ unsigned int r_mask[THREAD_NUM][10];
__shared__ unsigned int m[THREAD_NUM][10];

__shared__ unsigned int sum[THREAD_NUM];

const unsigned int t_mask = (1 << n) - 1;
int total = 0;
int i = 0;
unsigned int index;

if(idx < total_conditions) {
  mask[tid][i] = total_masks[idx];
  l_mask[tid][i] = total_l_masks[idx];
  r_mask[tid][i] = total_r_masks[idx];
  m[tid][i] = mask[tid][i] | l_mask[tid][i] | r_mask[tid][i];

  while(i >= 0) {
    if((m[tid][i] & t_mask) == t_mask) {
      i--;
    }
    else {
      index = (m[tid][i] + 1) & ~m[tid][i];
      m[tid][i] |= index;
      if((index & t_mask) != 0) {
        if(i + 1 == mark) {
          total++;
          i--;
        }
        else {
          mask[tid][i + 1] = mask[tid][i] | index;
          l_mask[tid][i + 1] = (l_mask[tid][i] | index) << 1;
          r_mask[tid][i + 1] = (r_mask[tid][i] | index) >> 1;
          m[tid][i + 1] = (mask[tid][i + 1] | l_mask[tid][i + 1] | r_mask[tid][i + 1]);
          i++;
        }
      }
      else {
        i --;
      }
    }
  }

  sum[tid] = total;
}
else {
  sum[tid] = 0;
}

__syncthreads();

// reduction
if(tid < 64 && tid + 64 < THREAD_NUM) { sum[tid] += sum[tid + 64]; } __syncthreads();
// DATARACE: if(tid < 32) { sum[tid] += sum[tid + 32 - 1]; } __syncthreads();
if(tid < 32) { sum[tid] += sum[tid + 32]; } __syncthreads();
if(tid < 16) { sum[tid] += sum[tid + 16]; } __syncthreads();
if(tid < 8) { sum[tid] += sum[tid + 8]; } __syncthreads();
if(tid < 4) { sum[tid] += sum[tid + 4]; } __syncthreads();
if(tid < 2) { sum[tid] += sum[tid + 2]; } __syncthreads();
if(tid < 1) { sum[tid] += sum[tid + 1]; } __syncthreads();

if(tid == 0) {
  results[bid] = sum[0];
}

}
