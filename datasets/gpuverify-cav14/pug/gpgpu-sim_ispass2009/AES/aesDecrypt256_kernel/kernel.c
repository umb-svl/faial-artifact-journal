#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif
#include "../sbox_D.h"
#include "../sbox_E.h"

/***************************************************************************
 *   Copyright (C) 2006                                                    *
 *                                                                         *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
/**
	@author Svetlin Manavski <svetlin@manavski.com>
 */
/* aes decryption operation:
 * Device code.
 *
 */
#define _AESDECRYPT_KERNEL_H_
// IMPERIAL EDIT: comment out C headers and add implicit includes and defines
//#include <stdio.h>
#define CUT_BANK_CHECKER( array, index)  array[index]
// Thread block size
#define BSIZE 256
#define STAGEBLOCK1(index)	CUT_BANK_CHECKER( stageBlock1, index )
#define STAGEBLOCK2(index)	CUT_BANK_CHECKER( stageBlock2, index )
#define TBOXE0(index)	    CUT_BANK_CHECKER( tBox0Block, index )
#define TBOXE1(index)		CUT_BANK_CHECKER( tBox1Block, index )
#define TBOXE2(index)		CUT_BANK_CHECKER( tBox2Block, index )
#define TBOXE3(index)		CUT_BANK_CHECKER( tBox3Block, index )
#define INVSBOX(index)		CUT_BANK_CHECKER( invSBoxBlock, index )
texture<unsigned, 1, cudaReadModeElementType> texDKey;





__global__ void kernel (unsigned* result, unsigned* inData, int inputSize) {




__requires(blockDim.x == 256);





__requires(gridDim.x == 128);





	unsigned bx		= blockIdx.x;
    unsigned tx		= threadIdx.x;
    unsigned mod4tx = tx%4;
    unsigned int4tx = tx/4;
    unsigned idx2	= int4tx*4;
	int x;
	unsigned keyElem;

    __shared__ UByte4 stageBlock1[BSIZE];
	__shared__ UByte4 stageBlock2[BSIZE];
    __shared__ UByte4 tBox0Block[BSIZE];
    __shared__ UByte4 tBox1Block[BSIZE];
    __shared__ UByte4 tBox2Block[BSIZE];
    __shared__ UByte4 tBox3Block[BSIZE];
    __shared__ UByte4 invSBoxBlock[BSIZE];
  
	// input caricati in memoria
	STAGEBLOCK1(tx).uival	= inData[BSIZE * bx + tx ];
	TBOXE0(tx).uival		= TBoxi0[tx];
	TBOXE1(tx).uival		= TBoxi1[tx];
	TBOXE2(tx).uival		= TBoxi2[tx];
	TBOXE3(tx).uival		= TBoxi3[tx];
	INVSBOX(tx).ubval[0]	= inv_SBox[tx];	
	
	__syncthreads();
	
	
	//----------------------------------- 1st stage -----------------------------------

	x = mod4tx;
	keyElem = tex1Dfetch(texDKey, x);
    STAGEBLOCK2(tx).uival = STAGEBLOCK1(tx).uival ^ keyElem;

	__syncthreads();
	
	//-------------------------------- end of 1st stage --------------------------------
	

	//----------------------------------- 2nd stage -----------------------------------
	
    int pidx = posIdx_D[16 + mod4tx*4]   + idx2;
    unsigned op1 = STAGEBLOCK2( pidx ).ubval[0];
	pidx = posIdx_D[16 + mod4tx*4+1] + idx2;
    unsigned op2 = STAGEBLOCK2( pidx ).ubval[1];
	pidx = posIdx_D[16 + mod4tx*4+2] + idx2;
    unsigned op3 = STAGEBLOCK2( pidx ).ubval[2];
    pidx = posIdx_D[16 + mod4tx*4+3] + idx2;
	unsigned op4 = STAGEBLOCK2( pidx ).ubval[3];
	
	op1 = TBOXE0(op1).uival;

    op2 = TBOXE1(op2).uival;
    
    op3 = TBOXE2(op3).uival;
    
    op4 = TBOXE3(op4).uival;

	x = mod4tx+4;
	keyElem = tex1Dfetch(texDKey, x);
	STAGEBLOCK1(tx).uival = op1^op2^op3^op4^keyElem;

	__syncthreads();

	//-------------------------------- end of 2nd stage --------------------------------

	//----------------------------------- 3th stage -----------------------------------
	pidx = posIdx_D[16 + mod4tx*4]   + idx2;
    op1 = STAGEBLOCK1( pidx ).ubval[0];
    pidx = posIdx_D[16 + mod4tx*4+1] + idx2;
	op2 = STAGEBLOCK1( pidx ).ubval[1];
    pidx = posIdx_D[16 + mod4tx*4+2] + idx2;
	op3 = STAGEBLOCK1( pidx ).ubval[2];
    pidx = posIdx_D[16 + mod4tx*4+3] + idx2;
	op4 = STAGEBLOCK1( pidx ).ubval[3];
	
	op1 = TBOXE0(op1).uival;

    op2 = TBOXE1(op2).uival;
    
    op3 = TBOXE2(op3).uival;
    
    op4 = TBOXE3(op4).uival;

	x = mod4tx+8;
	keyElem = tex1Dfetch(texDKey, x);
	 STAGEBLOCK2(tx).uival = op1^op2^op3^op4^keyElem;

	__syncthreads();

	//-------------------------------- end of 3th stage --------------------------------

	//----------------------------------- 4th stage -----------------------------------
    pidx = posIdx_D[16 + mod4tx*4]   + idx2;
    op1 = STAGEBLOCK2( pidx ).ubval[0];
	pidx = posIdx_D[16 + mod4tx*4+1] + idx2;
    op2 = STAGEBLOCK2( pidx ).ubval[1];
    pidx = posIdx_D[16 + mod4tx*4+2] + idx2;
	op3 = STAGEBLOCK2( pidx ).ubval[2];
    pidx = posIdx_D[16 + mod4tx*4+3] + idx2;
	op4 = STAGEBLOCK2( pidx ).ubval[3];
	
	op1 = TBOXE0(op1).uival;

    op2 = TBOXE1(op2).uival;
    
    op3 = TBOXE2(op3).uival;
    
    op4 = TBOXE3(op4).uival;

	x = mod4tx+12;
	keyElem = tex1Dfetch(texDKey, x);
	 STAGEBLOCK1(tx).uival = op1^op2^op3^op4^keyElem;

	__syncthreads();

	//-------------------------------- end of 4th stage --------------------------------

	//----------------------------------- 5th stage -----------------------------------
    pidx = posIdx_D[16 + mod4tx*4]   + idx2;
    op1 = STAGEBLOCK1( pidx ).ubval[0];
    pidx = posIdx_D[16 + mod4tx*4+1] + idx2;
	op2 = STAGEBLOCK1( pidx ).ubval[1];
    pidx = posIdx_D[16 + mod4tx*4+2] + idx2;
	op3 = STAGEBLOCK1( pidx ).ubval[2];
    pidx = posIdx_D[16 + mod4tx*4+3] + idx2;
	op4 = STAGEBLOCK1( pidx ).ubval[3];
	
	op1 = TBOXE0(op1).uival;

    op2 = TBOXE1(op2).uival;
    
    op3 = TBOXE2(op3).uival;
    
    op4 = TBOXE3(op4).uival;

	x = mod4tx+16;
	keyElem = tex1Dfetch(texDKey, x);
	 STAGEBLOCK2(tx).uival = op1^op2^op3^op4^keyElem;

	__syncthreads();

	//-------------------------------- end of 5th stage --------------------------------
	
	//----------------------------------- 6th stage -----------------------------------
    pidx = posIdx_D[16 + mod4tx*4]   + idx2;
    op1 = STAGEBLOCK2( pidx ).ubval[0];
    pidx = posIdx_D[16 + mod4tx*4+1] + idx2;
	op2 = STAGEBLOCK2( pidx ).ubval[1];
	pidx = posIdx_D[16 + mod4tx*4+2] + idx2;
    op3 = STAGEBLOCK2( pidx ).ubval[2];
    pidx = posIdx_D[16 + mod4tx*4+3] + idx2;
	op4 = STAGEBLOCK2( pidx ).ubval[3];
	
	op1 = TBOXE0(op1).uival;

    op2 = TBOXE1(op2).uival;
    
    op3 = TBOXE2(op3).uival;
    
    op4 = TBOXE3(op4).uival;

	x = mod4tx+20;
	keyElem = tex1Dfetch(texDKey, x);
	 STAGEBLOCK1(tx).uival = op1^op2^op3^op4^keyElem;

	__syncthreads();

	//-------------------------------- end of 6th stage --------------------------------

	//----------------------------------- 7th stage -----------------------------------
    pidx = posIdx_D[16 + mod4tx*4]   + idx2;
    op1 = STAGEBLOCK1( pidx ).ubval[0];
    pidx = posIdx_D[16 + mod4tx*4+1] + idx2;
	op2 = STAGEBLOCK1( pidx ).ubval[1];
    pidx = posIdx_D[16 + mod4tx*4+2] + idx2;
	op3 = STAGEBLOCK1( pidx ).ubval[2];
    pidx = posIdx_D[16 + mod4tx*4+3] + idx2;
	op4 = STAGEBLOCK1( pidx ).ubval[3];
	
	op1 = TBOXE0(op1).uival;

    op2 = TBOXE1(op2).uival;
    
    op3 = TBOXE2(op3).uival;
    
    op4 = TBOXE3(op4).uival;

	x = mod4tx+24;
	keyElem = tex1Dfetch(texDKey, x);
	STAGEBLOCK2(tx).uival = op1^op2^op3^op4^keyElem;

	__syncthreads();

	//-------------------------------- end of 7th stage --------------------------------
	
	//----------------------------------- 8th stage -----------------------------------
       
    pidx = posIdx_D[16 + mod4tx*4]   + idx2;
    op1 = STAGEBLOCK2( pidx ).ubval[0];
	pidx = posIdx_D[16 + mod4tx*4+1] + idx2;
    op2 = STAGEBLOCK2( pidx ).ubval[1];
    pidx = posIdx_D[16 + mod4tx*4+2] + idx2;
	op3 = STAGEBLOCK2( pidx ).ubval[2];
    pidx = posIdx_D[16 + mod4tx*4+3] + idx2;
	op4 = STAGEBLOCK2( pidx ).ubval[3];
	
	op1 = TBOXE0(op1).uival;

    op2 = TBOXE1(op2).uival;
    
    op3 = TBOXE2(op3).uival;
    
    op4 = TBOXE3(op4).uival;

	x = mod4tx+28;
	keyElem = tex1Dfetch(texDKey, x);
	STAGEBLOCK1(tx).uival = op1^op2^op3^op4^keyElem;

	__syncthreads();

	//-------------------------------- end of 8th stage --------------------------------
	
	//----------------------------------- 9th stage -----------------------------------
      
    pidx = posIdx_D[16 + mod4tx*4]   + idx2;
    op1 = STAGEBLOCK1( pidx ).ubval[0];
    pidx = posIdx_D[16 + mod4tx*4+1] + idx2;
	op2 = STAGEBLOCK1( pidx ).ubval[1];
    pidx = posIdx_D[16 + mod4tx*4+2] + idx2;
	op3 = STAGEBLOCK1( pidx ).ubval[2];
    pidx = posIdx_D[16 + mod4tx*4+3] + idx2;
	op4 = STAGEBLOCK1( pidx ).ubval[3];
	
	op1 = TBOXE0(op1).uival;

    op2 = TBOXE1(op2).uival;
    
    op3 = TBOXE2(op3).uival;
    
    op4 = TBOXE3(op4).uival;

	x = mod4tx+32;
	keyElem = tex1Dfetch(texDKey, x);
	STAGEBLOCK2(tx).uival = op1^op2^op3^op4^keyElem;

	__syncthreads();

	//-------------------------------- end of 9th stage --------------------------------

	//----------------------------------- 10th stage -----------------------------------
       
    pidx = posIdx_D[16 + mod4tx*4]   + idx2;
    op1 = STAGEBLOCK2( pidx ).ubval[0];
    pidx = posIdx_D[16 + mod4tx*4+1] + idx2;
	op2 = STAGEBLOCK2( pidx ).ubval[1];
    pidx = posIdx_D[16 + mod4tx*4+2] + idx2;
	op3 = STAGEBLOCK2( pidx ).ubval[2];
    pidx = posIdx_D[16 + mod4tx*4+3] + idx2;
	op4 = STAGEBLOCK2( pidx ).ubval[3];
	
	op1 = TBOXE0(op1).uival;

    op2 = TBOXE1(op2).uival;
    
    op3 = TBOXE2(op3).uival;
    
    op4 = TBOXE3(op4).uival;

	x = mod4tx+36;
	keyElem = tex1Dfetch(texDKey, x);
	STAGEBLOCK1(tx).uival = op1^op2^op3^op4^keyElem;

	__syncthreads();

	//-------------------------------- end of 10th stage --------------------------------

	//----------------------------------- 11th stage -----------------------------------
       
    pidx = posIdx_D[16 + mod4tx*4]   + idx2;
    op1 = STAGEBLOCK1( pidx ).ubval[0];
    pidx = posIdx_D[16 + mod4tx*4+1] + idx2;
	op2 = STAGEBLOCK1( pidx ).ubval[1];
	pidx = posIdx_D[16 + mod4tx*4+2] + idx2;
    op3 = STAGEBLOCK1( pidx ).ubval[2];
    pidx = posIdx_D[16 + mod4tx*4+3] + idx2;
	op4 = STAGEBLOCK1( pidx ).ubval[3];
	
	op1 = TBOXE0(op1).uival;

    op2 = TBOXE1(op2).uival;
    
    op3 = TBOXE2(op3).uival;
    
    op4 = TBOXE3(op4).uival;

	x = mod4tx+40;
	keyElem = tex1Dfetch(texDKey, x);
	STAGEBLOCK2(tx).uival = op1^op2^op3^op4^keyElem;

	__syncthreads();

	//-------------------------------- end of 11th stage --------------------------------
	
	//----------------------------------- 12th stage -----------------------------------
       
    pidx = posIdx_D[16 + mod4tx*4]   + idx2;
    op1 = STAGEBLOCK2( pidx ).ubval[0];
    pidx = posIdx_D[16 + mod4tx*4+1] + idx2;
	op2 = STAGEBLOCK2( pidx ).ubval[1];
    pidx = posIdx_D[16 + mod4tx*4+2] + idx2;
	op3 = STAGEBLOCK2( pidx ).ubval[2];
    pidx = posIdx_D[16 + mod4tx*4+3] + idx2;
	op4 = STAGEBLOCK2( pidx ).ubval[3];
	
	op1 = TBOXE0(op1).uival;

    op2 = TBOXE1(op2).uival;
    
    op3 = TBOXE2(op3).uival;
    
    op4 = TBOXE3(op4).uival;

	x = mod4tx+44;
	keyElem = tex1Dfetch(texDKey, x);
	STAGEBLOCK1(tx).uival = op1^op2^op3^op4^keyElem;

	__syncthreads();

	//-------------------------------- end of 12th stage --------------------------------
	
	//----------------------------------- 13th stage -----------------------------------
       
    pidx = posIdx_D[16 + mod4tx*4]   + idx2;
    op1 = STAGEBLOCK1( pidx ).ubval[0];
    pidx = posIdx_D[16 + mod4tx*4+1] + idx2;
	op2 = STAGEBLOCK1( pidx ).ubval[1];
    pidx = posIdx_D[16 + mod4tx*4+2] + idx2;
	op3 = STAGEBLOCK1( pidx ).ubval[2];
    pidx = posIdx_D[16 + mod4tx*4+3] + idx2;
	op4 = STAGEBLOCK1( pidx ).ubval[3];
	
	op1 = TBOXE0(op1).uival;

    op2 = TBOXE1(op2).uival;
    
    op3 = TBOXE2(op3).uival;
    
    op4 = TBOXE3(op4).uival;

	x = mod4tx+48;
	keyElem = tex1Dfetch(texDKey, x);
	STAGEBLOCK2(tx).uival = op1^op2^op3^op4^keyElem;

	__syncthreads();

	//-------------------------------- end of 13th stage --------------------------------
	
	//----------------------------------- 14th stage -----------------------------------
       
    pidx = posIdx_D[16 + mod4tx*4]   + idx2;
    op1 = STAGEBLOCK2( pidx ).ubval[0];
    pidx = posIdx_D[16 + mod4tx*4+1] + idx2;
	op2 = STAGEBLOCK2( pidx ).ubval[1];
    pidx = posIdx_D[16 + mod4tx*4+2] + idx2;
	op3 = STAGEBLOCK2( pidx ).ubval[2];
    pidx = posIdx_D[16 + mod4tx*4+3] + idx2;
	op4 = STAGEBLOCK2( pidx ).ubval[3];
	
	op1 = TBOXE0(op1).uival;

    op2 = TBOXE1(op2).uival;
    
    op3 = TBOXE2(op3).uival;
    
    op4 = TBOXE3(op4).uival;

	x = mod4tx+52;
	keyElem = tex1Dfetch(texDKey, x);
	STAGEBLOCK1(tx).uival = op1^op2^op3^op4^keyElem;

	__syncthreads();

	//-------------------------------- end of 14th stage --------------------------------
	
	//----------------------------------- 15th stage -----------------------------------

    pidx = posIdx_D[16 + mod4tx*4]   + idx2;
    op1 = STAGEBLOCK1( pidx ).ubval[0];
    pidx = posIdx_D[16 + mod4tx*4+1] + idx2;
	op2 = STAGEBLOCK1( pidx ).ubval[1];
    pidx = posIdx_D[16 + mod4tx*4+2] + idx2;
	op3 = STAGEBLOCK1( pidx ).ubval[2];
    pidx = posIdx_D[16 + mod4tx*4+3] + idx2;
	op4 = STAGEBLOCK1( pidx ).ubval[3];

	x = mod4tx+56;
	keyElem = tex1Dfetch(texDKey, x);

	STAGEBLOCK2(tx).ubval[3] = INVSBOX(op4).ubval[0]^( keyElem>>24);
	STAGEBLOCK2(tx).ubval[2] = INVSBOX(op3).ubval[0]^( keyElem>>16 & 0x000000FF);
	STAGEBLOCK2(tx).ubval[1] = INVSBOX(op2).ubval[0]^( keyElem>>8  & 0x000000FF);
	STAGEBLOCK2(tx).ubval[0] = INVSBOX(op1).ubval[0]^( keyElem     & 0x000000FF);

	__syncthreads();

	//-------------------------------- end of 15th stage --------------------------------

	result[BSIZE * bx + tx] = STAGEBLOCK2(tx).uival;
	// end of AES
	

}
