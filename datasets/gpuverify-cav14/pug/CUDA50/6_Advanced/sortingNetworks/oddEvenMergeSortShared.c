#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif
#include "common.h"






__global__ void kernel (uint* d_DstKey, uint* d_DstVal, uint* d_SrcKey, uint* d_SrcVal, uint arrayLength,uint dir) {




__requires(blockDim.x == 512);





__requires(gridDim.x == 1024);





  
    //Shared memory storage for one or more small vectors
    __shared__ uint s_key[SHARED_SIZE_LIMIT];
    __shared__ uint s_val[SHARED_SIZE_LIMIT];

    //Offset to the beginning of subbatch and load data
    d_SrcKey += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;
    d_SrcVal += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;
    d_DstKey += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;
    d_DstVal += blockIdx.x * SHARED_SIZE_LIMIT + threadIdx.x;
    s_key[threadIdx.x +                       0] = d_SrcKey[                      0];
    s_val[threadIdx.x +                       0] = d_SrcVal[                      0];
    s_key[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = d_SrcKey[(SHARED_SIZE_LIMIT / 2)];
    s_val[threadIdx.x + (SHARED_SIZE_LIMIT / 2)] = d_SrcVal[(SHARED_SIZE_LIMIT / 2)];

    for (uint size = 2;
         size <= arrayLength; size <<= 1)
    {
        uint stride = size / 2;
        uint offset = threadIdx.x & (stride - 1);

        {
            __syncthreads();
            uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));
            Comparator(
                s_key[pos +      0], s_val[pos +      0],
                s_key[pos + stride], s_val[pos + stride],
                dir
            );
            stride >>= 1;
        }

        for (;
             stride > 0;
             stride >>= 1)
        {
            __syncthreads();
            uint pos = 2 * threadIdx.x - (threadIdx.x & (stride - 1));

            if (offset >= stride)
                Comparator(
                    s_key[pos - stride], s_val[pos - stride],
                    s_key[pos +      0], s_val[pos +      0],
                    dir
                );
        }
    }

    __syncthreads();
    d_DstKey[                      0] = s_key[threadIdx.x +                       0];
    d_DstVal[                      0] = s_val[threadIdx.x +                       0];
    d_DstKey[(SHARED_SIZE_LIMIT / 2)] = s_key[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];
    d_DstVal[(SHARED_SIZE_LIMIT / 2)] = s_val[threadIdx.x + (SHARED_SIZE_LIMIT / 2)];

}
