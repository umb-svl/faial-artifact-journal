//pass
//--blockDim=[32] --gridDim=[2]

#include <cuda.h>
#include "../common.h"






__global__ void kernel (int* g_index, float* g_partQ, int Ntotal) {



  
  int n = blockIdx.x * blockDim.x + threadIdx.x;
    
  if(n<Ntotal)
    g_partQ[n] = tex1Dfetch(t_Q, g_index[n]);
  

}
