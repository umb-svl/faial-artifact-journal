----- Data-race 1/1 valid -----
The following race is correctly reported.

last-iter.cu: error: possible write-write race on x[64]:

Write by thread 0 in thread block 0 (global id 0), last-iter.cu:21:29:
  x[threadIdx.x + blockDim.x] = 0; //  0: x[64] (racy write!)

Write by thread 63 in thread block 0 (global id 63), last-iter.cu:19:24:
  x[threadIdx.x + 1] = y;      // 63: x[64]

Bitwise values of parameters of 'kernel':
 n = 1
