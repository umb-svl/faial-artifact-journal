//pass
//--blockDim=[64] --gridDim=[1]

#include <cuda.h>






__global__ void kernel (int* x, int n) {



__requires(n > 0);

x[threadIdx.x + 1] = 0; // 0: x[1], 1: x[2], ...
for (int y = 0; y < n; y++) {
    x[threadIdx.x] = y; // 1: x[1], 2: x[2], ... (racy writes!)
    __syncthreads();
}

}
