----- Data-race 1/1 valid -----
The following race is correctly reported.

Array:   tile[0, 1]
T1 mode: R
T2 mode: W

-------------------
 Globals     Value 
-------------------
 blockIdx.x  0 
-------------------
 blockIdx.y  0 
-------------------
 nreps       2 
-------------------
 r           1 
-------------------

---------------------
 Locals       T1  T2 
---------------------
 i            0   0 
---------------------
 i1           0   0 
---------------------
 threadIdx.x  1   0 
---------------------
 threadIdx.y  0   1 
---------------------

