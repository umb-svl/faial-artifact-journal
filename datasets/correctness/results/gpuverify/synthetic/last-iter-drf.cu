//pass
//--blockDim=[64] --gridDim=[1]

#include <cuda.h>






__global__ void kernel (int* x, int n) {



__requires(n > 0);

for (int y = 0; y < n; y++) {
    __syncthreads();
    if (threadIdx.x < blockDim.x - 1) { // this fixes racyness in last-iter
        x[threadIdx.x + 1] = y;
    }
}
x[threadIdx.x + blockDim.x] = 0;

}
