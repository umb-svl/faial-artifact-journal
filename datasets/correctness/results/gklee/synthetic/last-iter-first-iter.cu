#include <stdio.h>
#define __requires(x) klee_assume(x)







__global__ void kernel (int* x, int n) {



__requires(n > 2);
__requires(n <= 5592384);

for (int y = 1; y <= n; y++) {
    for (int z = 1; z <= y; z++) {
        __syncthreads();
        x[threadIdx.x + y + z] = z; // (last iter) 1: x[1 + 2n], ...
    }
}
for (int w = n * 2; w < n * 3; w++) {
    x[threadIdx.x + w + 1] = w; //     (first iter) 0: x[1 + 2n], ... (racy)
    __syncthreads();
}

}
int main () {
    /* Declare scalar 'n' */
    int n;
    klee_make_symbolic(&n, sizeof(int), "n");
    
    /* Declare array 'x' */
    int *x;
    cudaMalloc((void**)&x, 16777216 * sizeof(int));
    dim3 grid_dim(1);
    dim3 block_dim(64);
    kernel<<< grid_dim, block_dim >>>(
        x,
        n
    );
    return 0;
}
