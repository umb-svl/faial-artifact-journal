#!/usr/bin/env python3
import jinja2
import yaml
import argparse
import sys
import random

from pathlib import Path

def load_tpl(tpl:Path):
    fs = jinja2.FileSystemLoader(searchpath=str(tpl.parent))
    return jinja2.Environment(loader=fs).get_template(tpl.name)

def get_module(template, data):
    tpl = load_tpl(template)
    return tpl.make_module(vars=data)

def generate(template, data):
    random.seed(data["size"])
    tpl = load_tpl(template)
    return tpl.render(**data)

def main():
    parser = argparse.ArgumentParser(description='Generate micro-benchmarks.')
    tpl_dir = Path(__file__).parent / 'tpl'
    CHOICES = list(f.stem for f in tpl_dir.glob('*.tpl'))
    CHOICES.sort()
    parser.add_argument('-o', dest='output', help="Where to store output file. Default: standard output")
    parser.add_argument('--array-size', type=int, default=1024, help="Symbolic-exec tools require a known array size. Default: %(default)s")
    parser.add_argument('--tool', '-t', choices=["gpuverify", "esbmc", "pug", "gklee" ,"sesa", "simulee"], default="gpuverify", help="Code-generation compatible with the given tool. Default: %(default)s")
    parser.add_argument('--size', dest="size", type=int, default=3, help="Set the problem size. Default: %(default)s")
    parser.add_argument('--block-dim', "-b", type=int, action='append', default=[], help="Accepts multiple dimension sizes. Default: [64]")
    parser.add_argument('--grid-dim', '-g', type=int, action='append', default=[], help="Accepts multiple grid sizes. Default: [1]")
    parser.add_argument('--loop-bound', type=int, default=9, help="The upper bound of generated loops. ESBMC requires a known upper bound. Value 0 makes the loop unspecified. Default: %(default)s")
    parser.add_argument('type', choices=CHOICES)
    args = parser.parse_args()
    if len(args.block_dim) == 0:
        args.block_dim = [64]
    if len(args.grid_dim) == 0:
        args.grid_dim = [1]
    tpl = tpl_dir / (args.type + ".tpl")
    out = sys.stdout if args.output is None else open(args.output, "w")
    try:
        out.write(generate(tpl, vars(args)))
        out.write("\n")
    finally:
        if args.output is not None:
            out.close()

if __name__ == '__main__':
    import sys
    main()
