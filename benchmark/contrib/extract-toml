#!/usr/bin/env python3

import argparse
import shlex
import sys
import json
import toml
import subprocess
from toml import TomlEncoder

def parse_args(line):
  parser = argparse.ArgumentParser()
  parser.add_argument("--blockDim", dest="block_dim", default=[])
  parser.add_argument("--gridDim", dest="grid_dim", default=[])
  parser.add_argument("--warp-sync", type=int)
  parser.add_argument("--no-inline", action="store_true")
  parser.add_argument("--only-intra-group", action="store_true")
  parser.add_argument("-D", dest="props", action="append")
  args = parser.parse_args(shlex.split(line))
  return args

def parse_params(lines):
    line1 = lines[0].lstrip("//").strip()
    line2 = lines[1].lstrip("//").strip()
    args = parse_args(line2)
    args.result = line1
    return {
        "grid_dim": json.loads(args.grid_dim),
        "block_dim": json.loads(args.block_dim),
        "pass": args.result.strip() == "pass",
    }


def find_global(lines):
    for idx, line in enumerate(lines):
        if "__global__" in line:
            return idx
    raise ValueError()


def split_body(lines):
    idx = find_global(lines)
    return lines[0:idx], lines[idx:]

def parse_header(lines):
    header = []
    includes = []
    for line in lines:
        if line.strip() == "":
            continue
        if line.startswith("#include") and '"' in line:
            _, line = line.split('"', 1)
            line, _ = line.split('"', 1)
            includes.append(line)
        else:
            header.append(line)
    return header, includes

def mk_entry(elem):
    ty, name = elem
    ty = ty.strip()
    name = name.strip()
    if name == "" or ty == "":
        raise ValueError()
    return {name: ty}

def parse_func_sig(data):
    data = data.split("(", 1)[1]
    data = data.split(")", 1)[0]
    scalars = []
    arrays = []
    for elem in data.split(","):
        if "*" in elem:
            arrays.append(mk_entry(elem.split("*")))
        else:
            try:
                scalars.append(mk_entry(elem.rsplit(" ", 1)))
            except ValueError:
                pass
    return dict(
        scalars = scalars,
        arrays = arrays,
    )

def parse_pre(line):
    line = line.split("(", 1)[1]
    line = line.rsplit(")", 1)[0]
    return line

def parse_body(lines):
    body = "".join(lines)
    func_sig, func_body = body.split("{", 1)
    func_sig = parse_func_sig(func_sig)
    func_body = func_body.rsplit("}", 1)[0]
    pre = []
    if "__requires" in func_body:
        body = []
        for line in func_body.split("\n"):
            if "__requires" in line:
                pre.append(parse_pre(line))
            else:
                body.append(line)
        func_sig["pre"] = pre
        func_body = body
    else:
        func_body = func_body.split("\n")
    return func_sig, func_body

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    parser.add_argument("-p", "--print", action="store_true", help="Print the generated file")
    parser.add_argument("-r", "--run", action="store_true", help="Run faial at the end to ensure it is correct")
    args = parser.parse_args()
    out = args.filename.rsplit(".", 1)[0]
    out += ".toml"
    with open(args.filename, 'r') as fp, open(out, "w") as fout:
        lines = list(fp)
        result = parse_params(lines)
        header, body = split_body(lines[2:])
        header, includes = parse_header(header)
        result["includes"] = includes
        func_sig, body = parse_body(body)
        result.update(func_sig)
        if len(header) > 0:
            result["header"] = "".join(header)
        result["body"] = "\n".join(body)
        enc = TomlEncoder()
        dump_str = enc.dump_funcs[str]
        def new_dump_str(v):
            if "\n" in v:
                return '"""' + v + '"""'
            else:
                return dump_str(v)
        enc.dump_funcs[str] = new_dump_str
        print(toml.dumps(result, encoder=enc), file=fout)
        if args.print:
            print(toml.dumps(result, encoder=enc))
    if args.run:
        subprocess.call(["./test-tools.py", "--tool", "faial", out])

if __name__ == '__main__':
  main()