#!/usr/bin/env python3
import argparse
import sys

def get_error(fp):
    for line in fp:
        if "could not open source file" in line:
            return 'error:include'
        if "'yices::type_checker_exception'" in line:
            return 'error:yices'
        if "template" in line:
            return 'error:template'
        if "texture" in line or "Texture" in line:
            return 'error:unsupported-api'
        if 'identifier "curandState" is undefined' in line:
            return 'error:unsupported-api'
        if "frontend_failed" in line:
            return 'error:parser'
        if "Found Races" in line:
            return 'racy'
        if "No races or synchronization errors were found" in line:
            return 'drf'
        if "Segmentation" in line:
            return 'error'
        if "Aborted" in line:
            return 'error'
    return 'unknown'

def parse(status, log):
    with open(log) as fp:
        err = get_error(fp)
    if status is None:
        return err
    if status == 124:
        return 'timeout'
    if err == 'drf':
        assert status == 0, status
    if err == 'racy':
        assert status == 0, status
    return err

def main():
    parser = argparse.ArgumentParser(description="Parse a pug log.")
    parser.add_argument('--status', type=int, help="The exit status.")
    parser.add_argument('log', help="The output of pug.")
    args = parser.parse_args()
    print(parse(args.status, args.log))

if __name__ == '__main__':
    main()
