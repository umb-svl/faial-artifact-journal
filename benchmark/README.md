# `run-benchmark.py`

See `run-benchmark.py --help` for usage.

Example:
```
$ ./run-benchmark.py -i all-cuda.txt --repeat 30 -o timings.csv -C  -b ../datasets/gpuverify-cav14/ -o timings.csv  -c faial '${filename}'
```

Here is an example of a configuration file [`../datasets/gpuverify-cav14/run-faial.yaml`](../datasets/gpuverify-cav14/run-faial.yaml):

```yaml
# Input filename
input: all-cuda.txt
# Output filename, a CSV file
output: timings.csv
# How many times we should
repeat: 1
# Base directory
base_dir: .
# Run the command in the base directory of given file
change_dir: true
# Run the following command
command: faial ${filename}
# Write a log file with the output of the command
log: output-${count}.log
```

Run it with:

```bash
$ cd ../datasets/gpuverify-cav14
$ ../../benchmark/run-benchmark.py -f run-faial.yaml
```
