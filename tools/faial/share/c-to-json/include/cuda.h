#ifndef CUDA_H

#define CUDA_H

#ifndef _PTRDIFF_T_DEFINED
typedef long int ptrdiff_t;
#define _PTRDIFF_T_DEFINED
#endif

#ifndef __cplusplus
#ifndef _WCHAR_T_DEFINED
typedef short wchar_t;
#define _WCHAR_T_DEFINED
#endif
#endif

// Appease mingw
#ifdef __need_wint_t
typedef short wint_t;
#endif

#ifndef NULL
#if defined(__cplusplus)
#define NULL 0
#else
#define NULL ((void *)0)
#endif
#endif

#ifndef size_t
typedef unsigned int size_t;
#endif

#ifndef int8_t
typedef signed char int8_t;
#endif

#ifndef int16_t
typedef signed short int16_t;
#endif

#ifndef int32_t
typedef signed int int32_t;
#endif

#ifndef uint8_t
typedef unsigned char uint8_t;
#endif

#ifndef uint16_t
typedef unsigned short uint16_t;
#endif

#ifndef uint32_t
typedef unsigned int uint32_t;
#endif

extern void __requires(bool);
extern bool __is_pow2(int);
/* ------- */

#define CUDART_PI_F             3.141592654f
#define _DEVICE_QUALIFIER extern
#define __mul24(x,y) ((x)*(y))
#define __umul24(x,y) ((x)*(y))
#define __read(p) true
#define __write(p) true
#define __other_int(p) p
#define __uniform_int(X) ((X) == __other_int(X))
#define __no_read(p) !__read(p)
#define __no_write(p) !__write(p)
#define __implies(e1, e2) (!(e1)||(e2))
#define __read_implies(p, e) __implies(__read(p), e)
#define __write_implies(p, e) __implies(__write(p), e)
#define __write_offset(p) p
#define __read_offset(p) p
#define __same_group true
#define NULL 0

#define __NOP ((void) 1)
#define  __non_temporal_loads_begin()     __NOP
#define  __non_temporal_loads_end()       __NOP
#define  __invariant(X)                   __NOP
#define  __global_invariant(X)            __NOP
#define  __function_wide_invariant(X)     __NOP
#define  __function_wide_candidate_invariant(X)     __NOP
#define  __candidate_invariant(X)         __NOP
#define  __candidate_global_invariant(X)  __NOP
#define  __ensures(X)                     __NOP
#define  __global_ensures(X)              __NOP
#define  __assert                         __requires


extern void __skip_start();
extern void __skip_end();

/* C-1 */
extern double log(double x);
extern double sqrt(double x);
extern double rsqrt(double x);
extern float tanhf(float x);

#define frexp(x, y) __NOP

extern float length(float v);
extern void __sincosf( float  x, float* sptr, float* cptr );
extern double pow(double x, double y);
extern float rsqrtf(float x);
extern float sqrtf(float x);
extern float exp(float x);
extern float expf(float x);
extern float __expf(float x);
extern float exp2f(float x);
extern float logf(float x);
extern float __logf(float x);
extern float log2f(float x);
extern float log10f(float x);
extern float log1pf(float x);
extern float fabsf(float x);
extern int __ffs(int x);
extern float abs(float x);
extern float sinf(float x);
extern float cosf(float x);
extern float tanf(float x);
extern double sin(double x);
extern double cos(double x);
extern double tan(double x);

extern float atanf(float x);

/* C-2 */
extern float __fdividef(float x, float y);
extern double round(double x);
extern double floor(double x);
extern float ceilf(float x);
extern float floorf(float x);
extern float fminf(float x, float y);

/* cuda_vectors.h */
extern float clamp(float f, float a, float b);

/* INTEGER INTRINSICS */
extern int __clz(int x);


/* ------- */
#define __constant__ __attribute__((annotate("constant")))
#define __device__ __attribute__((annotate("device")))
#define __global__ __attribute__((annotate("global")))
#define __shared__ __attribute__((annotate("shared")))
#define __host__ __attribute__((annotate("host")))

typedef unsigned short ushort;
typedef unsigned int uint;

/* See Table B-1 in CUDA Specification */
/* From vector_functions.h */

#define __MAKE_VECTOR_OPERATIONS(TYPE,NAME) \
  typedef struct {   \
    TYPE x;          \
  } NAME##1;         \
  typedef struct {   \
    TYPE x, y;       \
  } NAME##2;         \
  typedef struct {   \
    TYPE x, y, z;    \
  } NAME##3;         \
  typedef struct {   \
    TYPE x, y, z, w; \
  } NAME##4;         \
  __host__ __device__ static __inline__ NAME##1 make_##NAME##1(TYPE x) \
  { \
    return { x }; \
  } \
  __host__ __device__ static __inline__ NAME##2 make_##NAME##2(TYPE x, TYPE y) \
  { \
    return { x, y }; \
  } \
  __host__ __device__ static __inline__ NAME##2 make_##NAME##2(TYPE x) \
  { \
    return make_##NAME##2(x, x); \
  } \
  __host__ __device__ static __inline__ NAME##3 make_##NAME##3(TYPE x, TYPE y, TYPE z) \
  { \
    return { x, y, z }; \
  } \
  __host__ __device__ static __inline__ NAME##3 make_##NAME##3(TYPE x) \
  { \
    return make_##NAME##3(x, x, x); \
  } \
  __host__ __device__ static __inline__ NAME##4 make_##NAME##4(TYPE x, TYPE y, TYPE z, TYPE w) \
  { \
    return { x, y, z, w }; \
  } \
  __device__ static __inline__ NAME##4 make_##NAME##4(TYPE x) \
  { \
    return make_##NAME##4(x, x, x, x); \
  }

__MAKE_VECTOR_OPERATIONS(signed char,char)
__MAKE_VECTOR_OPERATIONS(unsigned char,uchar)
__MAKE_VECTOR_OPERATIONS(short, short)
__MAKE_VECTOR_OPERATIONS(unsigned short,ushort)
__MAKE_VECTOR_OPERATIONS(int,int)
__MAKE_VECTOR_OPERATIONS(unsigned int,uint)
__MAKE_VECTOR_OPERATIONS(long,long)
__MAKE_VECTOR_OPERATIONS(unsigned long,ulong)
__MAKE_VECTOR_OPERATIONS(long long,longlong)
__MAKE_VECTOR_OPERATIONS(unsigned long long,ulonglong)
__MAKE_VECTOR_OPERATIONS(float,float)
__MAKE_VECTOR_OPERATIONS(double,double)

#undef __MAKE_VECTOR_OPERATIONS

/* from helper_math.h */

#define __MAKE_VECTOR_FROM_SIMILAR(TYPE,ZERO) \
  __host__ __device__ static __inline__ TYPE##2 make_##TYPE##2(TYPE##3 a) \
  { \
    return make_##TYPE##2(a.x, a.y);  /* discards z */ \
  } \
  __host__ __device__ static __inline__ TYPE##3 make_##TYPE##3(TYPE##2 a) \
  { \
    return make_##TYPE##3(a.x, a.y, ZERO); \
  } \
  __host__ __device__ static __inline__ TYPE##3 make_##TYPE##3(TYPE##2 a, TYPE s) \
  { \
    return make_##TYPE##3(a.x, a.y, s); \
  } \
  __host__ __device__ static __inline__ TYPE##3 make_##TYPE##3(TYPE##4 a) \
  { \
    return make_##TYPE##3(a.x, a.y, a.z); \
  } \
  __host__ __device__ static __inline__ TYPE##4 make_##TYPE##4(TYPE##3 a) \
  { \
    return make_##TYPE##4(a.x, a.y, a.z, ZERO); \
  } \
  __host__ __device__ static __inline__ TYPE##4 make_##TYPE##4(TYPE##3 a, TYPE w) \
  { \
    return make_##TYPE##4(a.x, a.y, a.z, w); \
  }

__MAKE_VECTOR_FROM_SIMILAR(float,0.0f)
__MAKE_VECTOR_FROM_SIMILAR(int,0)
__MAKE_VECTOR_FROM_SIMILAR(uint,0)

#undef __MAKE_VECTOR_FROM_SIMILAR

#define __MAKE_CONVERT(TYPE_TO,TYPE_FROM) \
  __host__ __device__ static __inline__ TYPE_TO##2 make_##TYPE_TO##2(TYPE_FROM##2 a) \
  { \
    return make_##TYPE_TO##2(TYPE_TO(a.x), TYPE_TO(a.y)); \
  } \
  __host__ __device__ static __inline__ TYPE_TO##3 make_##TYPE_TO##3(TYPE_FROM##3 a) \
  { \
    return make_##TYPE_TO##3(TYPE_TO(a.x), TYPE_TO(a.y), TYPE_TO(a.z)); \
  } \
  __host__ __device__ static __inline__ TYPE_TO##4 make_##TYPE_TO##4(TYPE_FROM##4 a) \
  { \
    return make_##TYPE_TO##4(TYPE_TO(a.x), TYPE_TO(a.y), TYPE_TO(a.z), TYPE_TO(a.w)); \
  }

__MAKE_CONVERT(float,int)
__MAKE_CONVERT(float,uint)
__MAKE_CONVERT(int,uint)
__MAKE_CONVERT(int,float)
__MAKE_CONVERT(uint,int)

#undef __MAKE_CONVERT

inline __device__ float3 operator/(float3 b, float a) {
  return make_float3(a/b.x,a/b.y,a/b.z);
}
inline __device__ float4 operator/(float4 b, float a) {
  return make_float4(a/b.x,a/b.y,a/b.z,a/b.w);
}
inline __device__ float3 operator/(float3 a,float3 b) {
  return make_float3(a.x/b.x,a.y/b.y,a.z/b.z);
}
inline __device__ float4 operator/(float4 a,float4 b) {
  return make_float4(a.x/b.x,a.y/b.y,a.z/b.z, a.w/b.w);
}

inline __device__ float4 operator*(float4 b, float a) {
  return make_float4(a*b.x,a*b.y,a*b.z,a*b.w);
}
inline __device__ float3 operator*(float3 b, float a) {
  return make_float3(a*b.x,a*b.y,a*b.z);
}
inline __device__ float4 operator*(float a,float4 b) {
  return make_float4(a*b.x,a*b.y,a*b.z,a*b.w);
}
inline __device__ float3 operator*(float a,float3 b) {
  return make_float3(a*b.x,a*b.y,a*b.z);
}
inline __device__ float2 operator*(float a,float2 b) {
  return make_float2(a*b.x,a*b.y);
}
inline __device__ float4 operator*(float4 a,float4 b) {
  return make_float4(a.x*b.x,a.y*b.y,a.z*b.z,a.w*b.w);
}
inline __device__ float3 operator*(float3 a,float3 b) {
  return make_float3(a.x*b.x,a.y*b.y,a.z*b.z);
}

inline __device__ float4 operator+(float4 a,float4 b) {
  return make_float4(a.x+b.x,a.y+b.y,a.z+b.z,a.w+b.w);
}
inline __device__ float3 operator+(float3 a,float3 b) {
  return make_float3(a.x+b.x,a.y+b.y,a.z+b.z);
}
inline __device__ int3 operator+(int3 a,int3 b) {
  return make_int3(a.x+b.x,a.y+b.y,a.z+b.z);
}
inline __device__ float2 operator+(float2 a,float2 b) {
  return make_float2(a.x+b.x,a.y+b.y);
}

inline __device__ float4 operator-(float4 a,float4 b) {
  return make_float4(a.x-b.x,a.y-b.y,a.z-b.z,a.w-b.w);
}
inline __device__ float3 operator-(float3 a,float3 b) {
  return make_float3(a.x-b.x,a.y-b.y,a.z-b.z);
}
inline __device__ float2 operator-(float2 a,float2 b) {
  return make_float2(a.x-b.x,a.y-b.y);
}

inline __device__ void operator*=(float4 &a, float b) {
  a = make_float4(a.x * b, a.y * b, a.z * b, a.w * b);
}

inline __device__ void operator+=(float4 &a, float b) {
  a = make_float4(a.x + b, a.y + b, a.z + b, a.w + b);
}
inline __device__ void operator+=(float3 &a, float b) {
  a = make_float3(a.x + b, a.y + b, a.z + b);
}
inline __device__ void operator+=(float3 &a, float3 b) {
  a = make_float3(a.x + b.x, a.y + b.y, a.z + b.z);
}
inline __device__ void operator+=(float4 &a, float4 b) {
  a = make_float4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
}

inline __device__ void operator-=(float4 &a, float b) {
  a = make_float4(a.x - b, a.y - b, a.z - b, a.w - b);
}
inline __device__ void operator-=(float3 &a, float b) {
  a = make_float3(a.x - b, a.y - b, a.z - b);
}
inline __device__ void operator-=(float3 &a, float3 b) {
  a = make_float3(a.x - b.x, a.y - b.y, a.z - b.z);
}
inline __device__ void operator-=(float4 &a, float4 b) {
  a = make_float4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
}

inline __device__ void operator*=(float4 &a, float4 b) {
  a = make_float4(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w);
}

struct __attribute__((device_builtin)) dim3
{
    unsigned int x, y, z;
};
typedef __attribute__((device_builtin)) struct dim3 dim3;

struct __attribute__((device_builtin)) dim4
{
    unsigned int x, y, z, w;
};
typedef __attribute__((device_builtin)) struct dim4 dim4;

extern float length(float1 v);
extern float length(float2 v);
extern float length(float3 v);
extern float length(float4 v);

extern float dot(float  a, float  b);
extern float dot(float1 a, float1 b);
extern float dot(float2 a, float2 b);
extern float dot(float3 a, float3 b);
extern float dot(float4 a, float4 b);

extern float  max(float  a, float  b);
extern float1 max(float1 a, float1 b);
extern float2 max(float2 a, float2 b);
extern float3 max(float3 a, float3 b);
extern float4 max(float4 a, float4 b);

extern float  min(float  a, float  b);
extern float1 min(float1 a, float1 b);
extern float2 min(float2 a, float2 b);
extern float3 min(float3 a, float3 b);
extern float4 min(float4 a, float4 b);

extern float  lerp(float a, float b, float w);
extern float1 lerp(float1 a, float1 b, float1 w);
extern float2 lerp(float2 a, float2 b, float2 w);
extern float3 lerp(float3 a, float3 b, float3 w);
extern float4 lerp(float4 a, float4 b, float4 w);

extern float1 lerp(float1 a, float1 b, float w);
extern float2 lerp(float2 a, float2 b, float w);
extern float3 lerp(float3 a, float3 b, float w);
extern float4 lerp(float4 a, float4 b, float w);

extern float3 cross(float3 a, float3 b);

extern float fminf ( float  x, float  y );
extern float3 fminf ( float3  x, float3  y );
extern float fmaxf ( float  x, float  y );
extern float3 fmaxf ( float3  x, float3  y );

__device__ float fabs(float v);
__device__ float2 fabs(float2 v);
__device__ float3 fabs(float3 v);
__device__ float4 fabs(float4 v);

extern float  saturate(float x);
extern float1 saturate(float1 x);
extern float2 saturate(float2 x);
extern float3 saturate(float3 x);
extern float4 saturate(float4 x);

extern float normalize(float v);
__device__ float2 normalize(float2 v);
__device__ float3 normalize(float3 v);
__device__ float4 normalize(float4 v);

#define __saturatef saturate

uint3 __attribute__((device_builtin)) extern const threadIdx;
uint3 __attribute__((device_builtin)) extern const blockIdx;
dim3 __attribute__((device_builtin)) extern const blockDim;
dim3 __attribute__((device_builtin)) extern const gridDim;
int __attribute__((device_builtin)) extern const warpSize;

extern __attribute__((annotate("device"))) __attribute__((device_builtin)) void __syncthreads(void);
extern __device__ int atomicInc(int * address, int val);
extern __device__ unsigned int atomicInc(unsigned int * address, int val);

extern __device__ int atomicDec(int * address, int val);
extern __device__ unsigned int atomicDec(unsigned int * address, int val);

extern __device__ int atomicCAS(int * address, int val, int val2);
extern __device__ int atomicAnd(int * address, int val);
extern __device__ int atomicOr(int * address, int val);
extern __device__ int atomicXor(int * address, int val);
extern __device__ int atomicMin(int * address, int val);
extern __device__ int atomicMax(int * address, int val);
extern __device__ int atomicExch(int * address, int val);
extern __device__ int atomicSub(int * address, int val);
extern __device__ int atomicAdd(int * address, int val);
extern __device__ unsigned int atomicAdd(unsigned int * address, int val);
extern __device__ long long atomicAdd(long long* address, int val);
extern __device__ unsigned long long atomicAdd(unsigned long long* address, int val);

#endif /* CUDA_H */
