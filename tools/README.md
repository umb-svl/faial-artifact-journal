# Faial:
* Source: ../source/
* Binaries: ./faial/

# GPUVerify
* Source: https://github.com/mc-imperial/gpuverify/releases/download/2018-03-22/GPUVerifyLinux64.zip
* Version: 2018-03-22
* Paper: https://www.doc.ic.ac.uk/~afd/homepages/papers/pdfs/2012/OOPSLA.pdf
* Usage:
  ```
  $ cd tools/gpuverify-2018-03-22
  $ python3 gvtester.py --from-file ../../examples/kernels.txt ../..
  ```

# GKLEE + SESA
* Source: in ./gklee-geof23, from https://github.com/Geof23/Gklee
* Binaries: in ./gklee-f77577
* Hash of git commit used: f77577343c67a3f1815ce5e802e2c933f8df876a
* GKLEE Paper: http://formalverification.cs.utah.edu/pdf/PPoPP12-GKLEE-Extended-Version.pdf
* SESA Paper: https://lipeng28.github.io/papers/sc14-sesa.pdf


# ESBMC-GPU
* Source: https://github.com/ssvlab/esbmc-gpu
* Hash: 408a1f83585c38437084a6bed2a721789651cabc
* Depends on libz3-4
* Paper: https://ssvlab.github.io/lucasccordeiro/papers/cppe2017.pdf

# PUG
* Source (binary): http://formalverification.cs.utah.edu/PUG/distributions/pug-v0.2_x64.tar.gz
* Version: 0.2
* Paper: http://formalverification.cs.utah.edu/PUG/papers/fse10-pug.pdf
* [PUG User Manual](http://formalverification.cs.utah.edu/PUG/docs/pug-user-manual-v0.2.pdf)
* **PUG input requires the following modifications:**
  * Replace **existing** `#include "util.h"` with `#include “my_cutil.h”`.
  * Kernel function names must end with "kernel" or "Kernel".
  * Kernel filenames must contain "kernel" or "Kernel".
  * Kernel file extension must be `.c` or `.C` (for more advance programs).
  * Kernels must cater to ROSE's need, e.g. make explicit type casting on the array arguments of a function call (otherwise ROSE will fail).
  * Include any header files in the same directory as the kernel being verified.
* Usage:
  ```
  $ ./tools/pug-v0.2_x64/pug datasets/pug-ipdpsw12/cav/CUDA50/0_Simple/vectorAdd/vectorAddKernel.c
  ```

# Simulee
* Paper: https://personal.utdallas.edu/~lxz144130/publications/icse2020b.pdf
