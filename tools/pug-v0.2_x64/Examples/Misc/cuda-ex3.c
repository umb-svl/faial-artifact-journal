
#define N 4

#include "my_cutil.h"

__global__ void kernel1 (int *a, int b) {
__shared__ int temp[N];

int idx = blockIdx.x * blockDim.x + threadIdx.x;
if (idx < N) temp[idx] = a[idx-1] + b;

__syncthreads(); // A barrier

if (idx < N) a[idx] = temp[idx];}
