#include "my_cutil.h"

extern __shared__ int s[];

int f1(int i, int j) {
  int tmp;
  tmp = i + j;
  return tmp;
}

int3 f2(int3 p, int3* q, int n) {
  q[2].y = 2;
  int3 qa;
  qa.x = 1;
  qa = q[1];
  qa.z++;
  return qa;
}

void __global__ kernel (int *out) {
//  int id = threadIdx.x;
//  int nt = blockDim.x;

  int3 points[3];
  points[1].x = 2;  
  
  int3 pp;
//  points[2] = pp;
//  points[3] = points[2];

  int n = 1;
  points[2] = f2(points[1], points, n);

//  int k = f1(s[id],s[id+1]);
// __syncthreads();
  
}
