#!/bin/bash
# This script is for internal use to update the Faial binaries commited git.

# Go to the directory of this script.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd $DIR

# Run `git pull` to ensure the repository is up to date.
git pull || exit

# Download the newest `cav21` Faial binary bundle from gitlab.
cd faial
rm -f faial.tar.bz2
wget --content-disposition https://gitlab.com/umb-svl/faial/-/jobs/artifacts/cav21/raw/bundle/faial.tar.bz2?job=bundle
tar xvf faial.tar.bz2

# Check to see if anything changed.
if [ -z $(git status --porcelain .) ];
then
    # No changes - nothing to commit.
    echo "Faial binaries are already up to date with 'cav21' branches!"
else
    # Something in Faial has updated; commit it to git.
    echo "Updating Faial binaries:"
    git add .
    git commit -m "Updated Faial binaries."
    git push
    echo "Faial binaries updated!"
fi

# Return to the root of this repository.
cd ../..

# Suggest next steps.
echo "To update an existing Docker container, run 'git pull' inside it."
echo "To rebuild and push the container, run:"
echo "$ docker-compose build --no-cache faial-cav21 && docker-compose push faial-cav21"
