#!/usr/bin/env python3
import subprocess
import shutil
from pathlib import Path
import sys

def change_ext(filename, ext):
    return filename.parent / (filename.stem + ext)

def safe_run(cmd):
    print("RUN", " ".join(map(str, cmd)), file=sys.stderr)
    subprocess.run(cmd, check=True)

# Simulee needs to call binaries from inside the gklee directory; locate it.
def find_gklee():
    try:
        gklee = subprocess.check_output(['which', 'gklee-nvcc'], text=True).strip()
    except subprocess.CalledProcessError:
        sys.exit("simulee-exec error: gklee-nvcc not found in PATH")
    return Path(gklee).parent

def run(filename, args):
    gklee_dir = find_gklee()
    bc = change_ext(filename, ".bc")
    ll = change_ext(filename, ".ll")
    cc = gklee_dir / "gklee-nvcc"
    dis = gklee_dir / "bin" / "llvm-dis"
    simulee = Path(__file__).parent / "simulee.py"
    try:
        safe_run([cc, filename, "-o", bc])
        try:
            safe_run([dis, bc, "-o", ll])
            safe_run(["python2", simulee] + args + [ll])
        finally:
            cpp = Path.cwd() / change_ext(filename, ".cpp").name
            txt = Path.cwd() / change_ext(filename, ".kernelSet.txt").name
            to_remove = [
                bc,
                ll,
                cpp,
                txt,
            ]
            for file in to_remove:
                if file.exists():
                    print("DEL", file, file=sys.stderr)
                    if file.is_symlink():
                        file.unlink()
                    elif file.is_dir():
                        shutil.rmtree(file)
                    else:
                        file.unlink()
    except subprocess.CalledProcessError as e:
        sys.exit(e.returncode)
    except KeyboardInterrupt:
        sys.exit(255)

def main():
    import sys
    run(Path(sys.argv[1]), sys.argv[2:])

if __name__ == '__main__':
    main()
